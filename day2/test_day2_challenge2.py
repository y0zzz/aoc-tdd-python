# Import the day2_challenge2 function
from day2_challenge2 import day2_challenge2

def test_day2_challenge2():
    # Test case for Day 2, Challenge 2
    password_list = [
        "1-3 a: abcde",
        "1-3 b: cdefg",
        "2-9 c: ccccccccc",
    ]
    result = day2_challenge2(password_list)
    assert result == 1

