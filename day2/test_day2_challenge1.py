# test_day2_challenge1.py
from day2_challenge1 import day2_challenge1

def test_day2_challenge1():
    # Test case for Day 2, Challenge 1
    assert day2_challenge1(["1-3 a: abcde", "1-3 b: cdefg", "2-9 c: ccccccccc"]) == 2

