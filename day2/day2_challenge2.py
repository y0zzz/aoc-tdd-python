def day2_challenge2(password_list):
    valid_count = 0
    for policy_password in password_list:
        policy, password = policy_password.split(": ")
        positions, char = policy.split(" ")
        pos1, pos2 = map(int, positions.split("-"))

        if (password[pos1 - 1] == char) ^ (password[pos2 - 1] == char):
            valid_count += 1

    return valid_count

if __name__ == "__main__":
    # Specify the input filename
    input_filename = "input_day2"
    
    # Read the password list from the input file and call the function
    with open(input_filename, 'r') as file:
        password_list = [line.strip() for line in file.readlines()]
    
    print(day2_challenge2(password_list))
