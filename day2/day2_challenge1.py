# day2_challenge1.py

def day2_challenge1(password_list):
    valid_count = 0
    for entry in password_list:
        parts = entry.split()
        min_max = parts[0].split('-')
        min_count = int(min_max[0])
        max_count = int(min_max[1])
        letter = parts[1][0]
        password = parts[2]

        letter_count = password.count(letter)
        if min_count <= letter_count <= max_count:
            valid_count += 1

    return valid_count
