# test_day1_challenge1.py
from day1_challenge1 import day1_challenge1

def test_day1_challenge1():
    # Test case for Day 1, Challenge 1
    assert day1_challenge1([1721, 979, 366, 299, 675, 1456]) == 514579

