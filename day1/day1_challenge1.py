# day1_challenge1.py

def day1_challenge1(expense_report):
    for i in range(len(expense_report)):
        for j in range(i + 1, len(expense_report)):
            if expense_report[i] + expense_report[j] == 2020:
                answer = expense_report[i] * expense_report[j]
                print(f"The puzzle answer is: {answer}")
                return answer  
    return None

if __name__ == "__main__":
    # Specify the input filename
    input_filename = "input_day1" 
    
    # Read the expense report from the input file and call the function
    with open(input_filename, 'r') as file:
        expense_report = [int(line.strip()) for line in file.readlines()]
    
    day1_challenge1(expense_report)
