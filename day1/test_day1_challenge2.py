# Import the day1_challenge2 function
from day1_challenge2 import day1_challenge2

def test_day1_challenge2():
    # Test case for Day 1, Challenge 2
    expense_report = [1721, 979, 366, 299, 675, 1456]
    result = day1_challenge2(expense_report)
    assert result == 241861950
